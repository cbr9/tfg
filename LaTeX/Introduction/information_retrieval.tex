\subsection{Recuperación de Información y PLN}\label{subsec:recuperación-de-información}
La Recuperación de Información (RI) puede definirse como
\begin{displayquote}
	finding material (usually documents) of an unstructured nature (usually text) that satisfies an information need from within large collections (usually stored on computers)~\citep{manning2008introduction}.
\end{displayquote}
En otras palabras, es el campo ocupado de que los buscadores como Google funcionen.
Este campo ha hecho varias aportaciones que hoy día se encuentran en la base de la Lingüística Computacional y el Procesamiento del Lenguaje Natural.
Las siguientes secciones tratan de explicar estas aportaciones y su importancia.
\subsubsection{Modelo de Espacio Vectorial}\label{subsec:modelo-de-espacio-vectorial}
Un ordenador no sabe nada.
No puede procesar una definición lexicográfica de la manera en que lo hace un humano, por lo que necesitan información estructurada en una manera concreta ---de forma númerica, a poder ser.

Por esta razón~\cite{salton1975vector} propusieron un modelo basados en vectores ---un tipo de estructura algebraica.
%For this reason, models based on the Distributional Hypothesis have come to adopt vectors, a certain kind of algebraic object, to represent distributions.
%This is known as the Vector Space Model (VSM), originally devised for the field of Information Retrieval as an alternative to using lexical matching techniques~\citep{salton1975vector, sahlgren2001vector}.
Ejemplos de vectores son los siguientes:
\[
	\vec{a} = \begin{bmatrix} 1 \\ 2 \end{bmatrix}
	\vec{b} = \begin{bmatrix} 5 \\ 1 \end{bmatrix}
\]
\theoremstyle{definition}
\begin{definition}[Dimensiones]
	Un vector tiene \textit{dimensiones}, es decir, un número concreto de elementos, lo cual a menudo se denota como $\mathbb{R}^n$.
	En este caso $\vec{a}$ y $\vec{b}$ están en $\mathbb{R}^2$ ---el plano cartesiano.
	Este hecho significa que podemos representarlos gráficamente\footnote[1]{En $\mathbb{R}^2$ y $\mathbb{R}^3$ esto no supone un problema.
	En dimensiones superiores empieza a ser problemático.}:
		\begin{center}
			\begin{tikzpicture}
				\draw[thin,gray!40] (0,0) grid (6,5);
				\draw[<->] (0,0)--(6,0) node[right]{$x$};
				\draw[<->] (0,0)--(0,5) node[above]{$y$};
				\draw[line width=2pt,blue,-stealth](0,0)--(1,2) node[anchor=south west]{$\boldsymbol{a}$};
				\draw[line width=2pt,red,-stealth](0,0)--(5,1) node[anchor=north east]{$\boldsymbol{b}$};
			\end{tikzpicture}
		\end{center}
\end{definition}
\theoremstyle{definition}
\begin{definition}[Producto escalar]
	El producto escalar mide el grado en que dos vectores apuntan en la misma dirección.
	Se denota con un punto, como en $\vec{v_1} \cdot \vec{v_2}$.
	De manera algebraica se define como
	\[
		\vec{a} \cdot \vec{b} = \sum_{i=1}^{n}\vec{a}_i\vec{b}_i
	\]
\end{definition}
También tiene una definición geométrica, que veremos más tarde.
Para nuestros dos ejemplos de prueba:
\[\vec{a} \cdot \vec{b} = 1(5) + 2(1) = 7\]
\theoremstyle{definition}
\begin{definition}[Norma]
	La norma $p$ o \textit{$\ell_p$} de un vector, denotada $\Vert\vec{a}\Vert_p$ se define como
	\[
		\Vert\vec{a}\Vert_p = \sqrt[p]{\sum_{i=1}^{n}|\vec{a}_i|^p}
	\]
\end{definition}
donde $|\vec{a}_i|$ es el valor absoluto de $\vec{a}_i$.
\theoremstyle{definition}
\begin{definition}[Longitud]
	\label{length}
	La \textit{longitud}, \textit{magnitud} o \textit{norma $\ell_2$}
	\footnote[1]{Otra norma bastante frecuente en aplicaciones de PLN es la $\ell_1$, también llamada distancia Manhattan o distancia de Taxi, dado que representa distancias recorridas en línea recta, como en una ciudad.}
	(o simplemente la \textit{norma}) de un vector $\vec{a}$ es la longitud de la línea que forma.
	Se basa en el producto escalar \footnote{En $\mathbb{R}^2$, la raíz cuadrada del producto escalar es equivalente al teorema de Pitágoras, por lo que la norma $\ell_2$ representa su generalización en $n$ dimensiones} y se denota $\Vert\vec{a}\Vert$
	\[
		\Vert\vec{a}\Vert= \sqrt{\sum_{i=1}^{n}|\vec{a}_i|^2} = \sqrt{\vec{a} \cdot \vec{a}}
	\]
\end{definition}
Aplicado a nuuestros ejemplos:
\begin{gather*}
	\Vert\vec{a}\Vert = \sqrt{1(1) + 2(2)} = \sqrt{1^2 + 2^2} = \sqrt{5} \approx \pgfmathparse{sqrt(5)}\pgfmathresult \\
	\Vert\vec{b}\Vert = \sqrt{5(5) + 1(1)} = \sqrt{5^2 + 1^2} = \sqrt{26} \approx \pgfmathparse{sqrt(26)}\pgfmathresult \\
\end{gather*}
%    This is useful when we wish to normalize it to \textit{unit length}, which creates a \textit{unit vector}, denoted $\vec{u}$
%    \[\vec{u} = \frac{\vec{a}}{\Vert\vec{a}\Vert}\]
\theoremstyle{definition}
\begin{definition}[Coseno]
	El coseno es el valor $x$ de un ángulo cuando cruza la circunferencia de un círculo de radio 1.
\end{definition}
	Por ejemplo, $\cos(0\degree) = 1$, porque no se eleva sobre el eje $x$, y $\cos(90\degree) = 1$, porque se mantiene sobre el eje $y$.
	%        include graph
\theoremstyle{definition}
\begin{definition}[Distancia coseno]
	La distancia coseno es una medida utilizada para comparar dos vectores con las mismas n dimensiones.
\end{definition}
	\[
		\vec{a} \cdot \vec{b} = \Vert\vec{a}\Vert\Vert\vec{b}\Vert\cos(\vec{a}, \vec{b})
	\]
Esta formula es la definición geométrica del producto escalar.
Con unas pocas operaciones de álgebra podemos obtener la formula para el coseno entre dos vectores:
\[\cos(\vec{a}, \vec{b}) = \frac{\vec{a} \cdot \vec{b}}{\Vert\vec{a}\Vert\Vert\vec{b}\Vert}\]
La cual podemos aplicar a nuestros ejemplos:
\[
	\cos(\vec{a}, \vec{b}) = \frac{7}{\sqrt{5}\sqrt{26}} \approx \pgfmathparse{7/(sqrt(5)*sqrt(26))}\pgfmathresult
\]
Previamente hemos dicho que el producto escalar mide el grado en que dos vectores apuntan en la misma dirección.
Esto debería resultar obvio a partir de esta definición, ya que podemos ver que se trata simplemente del coseno multiplicado por la magnitud de cada vector.
De manera gráfico, si tenemos dos \textit{vectores unitarios} ---dos vectores cuya longitud es igual a 1---, la formula se simplifica a
\[\cos(\vec{a}, \vec{b}) = \frac{\vec{a} \cdot \vec{b}}{1(1)} = \vec{a} \cdot \vec{b}\]

Dado que tanto los documentos como las palabras se representan como vectores, podemos utilizar esta medida para comparar su grado de similitud semántica.
Si $\cos(\vec{a}, \vec{b}) = 1$, esto significa que tenemos un ángulo de $0\degree$ y que por tanto los dos vectores (i.e. las dos palabras o documentos) apuntan en la misma dirección.
En aplicaciones de Lingüística Computacional, esto es extradamente raro, por lo que cuanto más se acerque el valor a 1, más relacionados estarán.
Un valor de 0 indica que los dos vectores son \textit{ortogonales} o \textit{perpendiculares}, y por tanto no están relacionados.
Los valores entre -1 y 0 por lo general se ignoran, ya que están demasiado alejados.

Hemos visto las operaciones básicas que podemos hacer sobre vectores, pero queda aún por explicar como convertimos palabras en este tipo de estructuras.
Cuando llevamos a cabo este proceso, llamamos a estos vectores \textit{word-vectors} o \textit{word embeddings}.
Las siguientes secciones revisan los métodos más simples.
\subsubsection{Matrices de coocurrencia}\label{subsec:textit}
Para representar un corpus en una sola estructura, utilizamos matrices, otro objeto que, como los vectores, proviene del álgebra lineal.
En una matriz, que de una forma simplista puede verse como una tabla, tenemos filas y columnas.
Estas filas y columnas son a su vez vectores, que en este caso representan palabras o documentos.
Para construir este tipo de matrices, tenemos generalmente dos opciones.
\paragraph*{Matrices de palabras por documentos.}
En este caso, las filas representan palabras y las columnas, documentos.
Este tipo de matrices se utilizan a menudo en Recuperación de Información, ya que permiten tres tipos de comparaciones~\citep{landauer1997solution}, principalmente
\begin{itemize}
	\item \textbf{Entre documentos y palabras}, utilizando la celda situada en la intersección,
	\item \textbf{Entre palabras}, mediante la distancia coseno\footnote[1]{
	Podemos utilizar otros tipos de medidas de distancia.
	La distancia euclídea, la distancia de Jaccard, o la distancia de Dice son bastantes conocidas.
	Sin embargo, el coseno suele devolver los mejores resultados.} de dos filas distintas, y
	\item \textbf{Entre documentos}, mediante el coseno de dos columnas.
\end{itemize}
\paragraph*{Matrices de palabra por palabra.}
En este tipo de matrices, tanto las filas como las columnas representan palabras.
La fila número $i$ de la matriz representa una palabra, mientras que la columna $j$ representa una palabra que aparece (o no) en los contextos en que aparece la palabra $i$.
Llamaremos a estas palabras $w_i$ y $w_j$, respectivamente.

Dado que necesitamos alguna manera para contar las apariciones de $w_j$ en estos contextos, debemos primero definir lo que consideraremos un contexto (o cotexto, ya que no tenemos acceso a conocimiento no textual), ya que podemos elegir cualquier extensión, desde el documento entero, pasando por un párrafo o una frase hasta llegar a unas pocas palabras.
Esta última opción es manejada por muchos estudios.
El procedimiento es el de establecer una ventana de $n$ palabras a la izquierda de $w_i$ y $k$ palabras a la derecha de $w_i$, siendo $n=2$ y $k=2$ opciones que dan buenos resultados.

Tras dicha definición, podemos empezar a asignar valores a las celdas de la matriz\footnote[1]{Métodos más sofisticados, como Word2Vec ---un modelo basado en \textit{deep learning} propuesto por~\cite{mikolov2013distributed}--- dan resultados mucho mejores.}.
Una opción es la de simplemente indicar con un 1 o un 0 si la palabra $w_j$ aparece o no en el contexto definido, respectivamente.
Los vectores resultantes serían \textit{vectores booleanos}.
Otra opción algo más compleja sería asignar a cada celda el número de veces que aparece $w_j$ en los contextos de $w_i$, lo cual se conoce como el modelo de \textit{bolsa de palabras}.

\subsubsection{TF-IDF}\label{subsec:tf-idf}
El TF-IDF es un método algo más sofisticado para la asignación de valores a estos vectores.
Al igual que el Modelo de Espacio Vectorial, tambíen proviene de la Recuperación de Información.
Consiste de dos partes:
\begin{itemize}
	\item La frecuencia absoluta\footnote[2]{Pueden utilizarse otras variantes.} del término (\textit{term-frequency} o tf;~\citep*{luhn1957statistical}) en un documento dado, y
	\item La inversa del número de documentos en que aparece el término (\textit{inverse document frequency} o idf;~\citep{jones1972statistical}), definido como
	\[
		idf(t, D)=\log\left(\frac{N}{|\lbrace d \in D: t \in d\rbrace|}\right)
	\]
	donde $N$ es el número de documentos del corpus y $|\lbrace d \in D: t \in d\rbrace|$ es el número de documentos en que aparece el término $t$.
\end{itemize}
\subsubsection{Precisión y cobertura}\label{subsec:precisión-y-cobertura}
