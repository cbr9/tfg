from typing import List, Tuple
from collections import Counter
from spacy.matcher import Matcher
import spacy
import numpy as np

nlp = spacy.load("en_core_web_sm")


class Collocations:
    def __init__(self, text):
        self.__text = text.split()
        self.__bigrams = Counter([(self.__text[index], self.__text[index + 1]) for index, word in enumerate(self.__text[:-1])])
        self.__unigrams = Counter([self.__text[index] for index, word in enumerate(self.__text)])
        self.__N = len(self.__text)
        self.prob_collocations = self.filter_collocations()

    def likelihood_ratio(self, c1: int, c2: int, c12: int) -> float:
        def probability(n: int, k: int, x: float) -> float:
            success = np.log(x) * k
            failure = np.log(1 - x) * (n - k)
            return success + failure
        p1 = c12 / c1
        p2 = (c2 - c12) / (self.__N - c1)
        p = c2 / self.__N
        H0 = probability(n=c1, k=c12, x=p) + probability(n=self.__N - c1, k=c2 - c12, x=p)
        HA = probability(n=c1, k=c12, x=p1) + probability(n=self.__N - c1, k=c2 - c12, x=p2)
        result = -2 * (H0 - HA)
        if np.isnan(result):
            result = 0
        return result

    def collocations(self) -> List[Tuple[float, str]]:
        collocations = []
        for bigram in self.__bigrams.keys():
            c1 = self.__unigrams[bigram[0]]
            c2 = self.__unigrams[bigram[1]]
            c12 = self.__bigrams[bigram]
            collocations.append((self.likelihood_ratio(c1=c1, c2=c2, c12=c12), " ".join(bigram)))
        collocations = sorted(collocations, reverse=True)
        return collocations

    def matcher(self):
        matcher = Matcher(nlp.vocab)
        pattern1 = [{"POS": "ADJ"}, {"POS": "NOUN"}]
        pattern2 = [{"POS": "NOUN"}, {"POS": "NOUN"}]
        pattern3 = [{"POS": "VERB"}, {"POS": "NOUN"}]
        pattern4 = [{"POS": "VERB"}, {"POS": "ADV"}]
        matcher.add("ADJ, NOUN", None, pattern1)
        matcher.add("NOUN, NOUN", None, pattern2)
        matcher.add("VERB, NOUN", None, pattern3)
        matcher.add("VERB, ADV", None, pattern4)
        return matcher 

    def filter_collocations(self) -> List[Tuple[float, str]]:
        collocations = list(set(self.collocations()))[:500]
        filtered_collocations = []
        matcher = self.matcher()
        for col in collocations:
            bigram = col[1]
            doc = nlp(bigram)
            stopwords = [token.is_stop for token in doc]
            if not any(stopwords): # if there's no true value in variable stopwords
                matches = matcher(doc=doc)
                if matches:
                    for match_id, start, end in matches:
                        matched_span = doc[start:end]
                        filtered_collocations.append((col[0], matched_span.text))
        filtered_collocations = sorted(filtered_collocations, reverse=True)
        return filtered_collocations


if __name__ == "__main__":
    with open(file="abc.txt", mode="r", encoding="utf8") as foods:
        foods = foods.read()
    test = Collocations(text=foods)
    for collocation in test.prob_collocations:
        print(collocation)
