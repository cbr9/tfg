import numpy as np
import matplotlib.pyplot as plt

# v1 = [1, 2]
# v2 = [5, 1]
#
# plt.plot([0,v2[0]],[0,v2[1]])
# plt.plot([0,v1[0]],[0,v1[1]])
# plt.legend()
# plt.axis('equal')
# plt.plot([-5, 5],[0, 0],'k--')
# plt.plot([0, 0],[-5, 5],'k--')
# plt.grid()
# plt.xlim(5)
# plt.ylim(5)
# plt.axis((-5, 5, -5, 5))
# plt.show()
#
from mpl_toolkits.axes_grid.axislines import SubplotZero
import matplotlib.pyplot as plt
import numpy as np

if 1:
	fig = plt.figure(1)
	ax = SubplotZero(fig, 111)
	fig.add_subplot(ax)
	circ = plt.Circle((0, 0), radius=1, edgecolor='b', facecolor='None')
	ax.add_patch(circ)
	for direction in ["xzero", "yzero"]:
		ax.axis[direction].set_axisline_style("-|>")
		ax.axis[direction].set_visible(True)

	for direction in ["left", "right", "bottom", "top"]:
		ax.axis[direction].set_visible(False)

	# x = np.linspace(-0.5, 1., 100)
	a = [1, 2]
	a /= np.linalg.norm(a)
	b = [5, 1]
	# projection_a_onto_b = (np.dot(a, b) / np.linalg.norm(b))*(b/np.linalg.norm(b))
	# print(projection_a_onto_b)
	b /= np.linalg.norm(b)
	cosine = (np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b)))
	sine = np.sqrt(1-cosine**2)
	plt.ylim([0, 1])
	plt.xlim([0, 1])
	# plt.yticks([0.5, 1, 1.5, 2])
	plt.plot([0,a[0]],[0,a[1]], label='a')
	plt.plot([0,b[0]],[0,b[1]], label='b')
	plt.scatter(cosine, sine)
	plt.legend()
	# ax.plot(x, np.sin(x*np.pi))

	plt.show()