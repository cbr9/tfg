from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize
import pandas as pd
import numpy as np
import re


with open(file="abc.txt", mode="r", encoding="utf-8") as f:
	news = f.read().split('\n\n')


def remove_numbers(string: str) -> str:
	regex = re.compile(r"\d+(?:\.\d+)?")
	return re.sub(pattern=regex, repl="", string=string)


def remove_punctuation(string: str) -> str:
	regex = re.compile(r"[»«,.:]")
	return re.sub(pattern=regex, repl="", string=string)


for i, piece_of_news in enumerate(news):
	data = pd.DataFrame(columns=["sentence", "title"])
	tokenized_text = sent_tokenize(remove_numbers(piece_of_news))
	for sentence in tokenized_text[1:]:
		data = data.append({
			'title': remove_punctuation(tokenized_text[0].lower().replace("\n", "")),
			'sentence': remove_punctuation(sentence.lower())
		}, ignore_index=True)

	print(tokenized_text[0])
	vectorizer = TfidfVectorizer(min_df=1, stop_words=stopwords.words('spanish'))
	bag_of_words = vectorizer.fit_transform(data.sentence)

	svd = TruncatedSVD(n_components=2)
	lsa = svd.fit_transform(bag_of_words)

	encoding_matrix = pd.DataFrame(data=svd.components_, index=['topic_1', 'topic_2']).T
	encoding_matrix['terms'] = vectorizer.get_feature_names()

	encoding_matrix['abs_topic_1'] = np.abs(encoding_matrix['topic_1'])
	encoding_matrix['abs_topic_2'] = np.abs(encoding_matrix['topic_2'])
	print(encoding_matrix.sort_values('abs_topic_1', ascending=False)[:10])
	# print(encoding_matrix.sort_values('abs_topic_2', ascending=False)[:10])
